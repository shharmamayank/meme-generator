import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import NavBar from './component/NavBar'
import './App.css'
import HeroSection from './component/HeroSection'

function App() {


  return (
    <>
      <NavBar />
      <HeroSection />


    </>

  )
}

export default App
